﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ClientServerControl
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private BackgroundWorker dataReceiveWorker;

        private const string LOCALHOST = "127.0.0.1";
        private const string REMOTEHOST = "192.168.2.89";

        Encoding Encoder = Encoding.UTF8;
        private UdpClient client;
        private IPEndPoint ep;
        private String sendText;
        private  byte[] receivedData;
        
        public enum LEDSTATUS
        {
            LEDON = 0, LEDOFF = 1, LEDFLASH = 2
        }

        public ObservableCollection<EventLog> EventLogList { get; set; }

        public string SendText {  get { return sendText; } set {
                if(sendText != value)
                {
                    sendText = value;
                    OnPropertyChanged("SendData");
                }
            } }

        private int selectedLEDStatus;

        public int SelectedLEDStatus
        {
            get { return selectedLEDStatus; }
            set
            {
                if (selectedLEDStatus != value)
                {
                    selectedLEDStatus = value;
                    OnPropertyChanged("SelectedLEDStatus");
                }
            }
        }

        public string GetSelectedLEDStatusString
        {
            get {
                string letStatusString = "";
                    switch (SelectedLEDStatus)
                {
                    case (int)LEDSTATUS.LEDOFF: letStatusString = "disable LED";
                        break;
                    case (int)LEDSTATUS.LEDON:
                        letStatusString = "enable LED";
                        break;
                    case (int)LEDSTATUS.LEDFLASH:
                        letStatusString = "flash LED";
                        break;
                };
                return letStatusString;
            }
        }

        #region Commands
        ICommand _sendDataCommand;
        ICommand _controlLEDStatusCommand;
        ICommand _closeCommand;

        public ICommand SendDataCommand
        {
            get
            {
                if (_sendDataCommand == null)
                {
                    _sendDataCommand =
                    new RelayCommand(param => this.SendDataCommand_Execute(param),
                   param => this.SendDataCommand_CanExecute(param));
                }
                return _sendDataCommand;
            }
        }

        bool SendDataCommand_CanExecute(object param)
        {
            return true;
        }

        /*send text to device*/
        void SendDataCommand_Execute(object param)
        {
            if (SendText.Length > 0)
            { 
            client.Send(Encoder.GetBytes(SendText), SendText.Length);
            EventLogList.Add(new EventLog { Timestamp = DateTime.Now, Direction = "TX", Message = "Text: " + SendText });      
        }
        }

        public ICommand ControlLEDStatusCommand
        {
            get
            {
                if (_controlLEDStatusCommand == null)
                {
                    _controlLEDStatusCommand =
                    new RelayCommand(param => this.ControlLEDStatusCommand_Execute(param),
                   param => this.ControlLEDStatusCommand_CanExecute(param));
                }
                return _controlLEDStatusCommand;
            }
        }

        bool ControlLEDStatusCommand_CanExecute(object param)
        {
            return true;
        }

        /*send request to change LED status to device*/
        void ControlLEDStatusCommand_Execute(object param)
        {
            client.Send(Encoder.GetBytes(SelectedLEDStatus.ToString()), SelectedLEDStatus.ToString().Length);
            EventLogList.Add(new EventLog { Timestamp = DateTime.Now, Direction = "TX", Message = GetSelectedLEDStatusString });     
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand =
                    new RelayCommand(param => this.CloseCommand_Execute(param),
                   param => this.CloseCommand_CanExecute(param));
                }
                return _closeCommand;
            }
        }

        bool CloseCommand_CanExecute(object param)
        {
            return true;
        }

        void CloseCommand_Execute(object param)
        {
            client.Close();
        }
        #endregion

        #region backgroundworker

        /*worker function that handles incoming data*/
        private void bgWorkerGetDataDoWork(object sender, DoWorkEventArgs e)
        {
            string receivedDataString;
            while (true)
            {
                receivedData = client.Receive(ref ep);
                receivedDataString = Encoder.GetString(receivedData);

                /*Add received data to log*/
                if (receivedDataString.Length > 0)
                {
                    Application.Current.Dispatcher.BeginInvoke((Action)(() => EventLogList.Add(new EventLog { Timestamp = DateTime.Now, Direction = "RX", Message = receivedDataString })));
                }
            }
        }
        #endregion

        #region constructors
        public MainWindowViewModel()
        {
            EventLogList = new ObservableCollection<EventLog>();
            SendText = "";
            SelectedLEDStatus = 0;
            receivedData = new byte[2];
            client = new UdpClient();
            
            ep = new IPEndPoint(IPAddress.Parse(REMOTEHOST), 9199);
            client.Connect(ep);

            /*create and start data receive thread*/
            dataReceiveWorker = new BackgroundWorker();
            dataReceiveWorker.DoWork += new DoWorkEventHandler(bgWorkerGetDataDoWork);
            dataReceiveWorker.WorkerSupportsCancellation = true;
            dataReceiveWorker.RunWorkerAsync();
        }
        #endregion

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler tempHandler = PropertyChanged;
            if (tempHandler != null)
                tempHandler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
