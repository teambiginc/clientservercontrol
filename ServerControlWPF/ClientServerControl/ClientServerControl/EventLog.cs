﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientServerControl
{
    public class EventLog
    {
        public DateTime Timestamp { get; set; }
        public string Direction { get; set; }
        public string Message { get; set; }
    }
}
