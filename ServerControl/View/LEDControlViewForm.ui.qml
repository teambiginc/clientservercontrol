import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
Item {
    id: item1
    width: 400
    height: 400

    Column {
        id: column1
        width: 400
        height: 400
        anchors.left: parent.left
        anchors.leftMargin: 0


        Row {
            id: row2
            width: 400
            height: 40
            anchors.top: parent.top
            anchors.topMargin: 0

            Label {
                id: label1
                text: qsTr("ServerAddress: ")
            }

            TextField {
                id: textField1
                height: 40
            }
        }

        Row {
            id: row3
            x: 0
            width: 400
            height: 40
            anchors.top: parent.top
            anchors.topMargin: 40

            Label {
                id: label2
                text: qsTr("Port: ")
            }

            TextField {
                id: textField2
                x: 77
            }
        }
        Row {
            id: row1
            width: 400
            height: 40
            anchors.top: parent.top
            anchors.topMargin: 80

            Button{
                text: qsTr("Connect")


            }

            Button {
                id: button1
                text: qsTr("Disconnect")
                anchors.left: parent.left
                anchors.leftMargin: 120

            }
        }

        Row {
            id: row4
            y: 12
            width: 400
            height: 40
            anchors.top: parent.top
            anchors.topMargin: 120

            Button {
                id: button2
                text: qsTr("Send")

            }
        }
    }

}
