import QtQuick 2.5

Rectangle {
    property alias mouseArea: mouseArea

    width: 600
    height: 600

    MouseArea {
        id: mouseArea
        width: 600
        height: 600
        anchors.fill: parent

        LEDControlViewForm {
            id: lEDControlViewForm1
            x: 20
            y: 179
            width: 300
            height: 300
        }
    }
}
