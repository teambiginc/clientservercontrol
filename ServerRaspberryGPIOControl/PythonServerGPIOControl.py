import sys
import time
import RPi.GPIO as GPIO
from socket import *
from subprocess import *

PIN = 47
LEDON = "0"
LEDOFF = "1"
LEDFLASH = "2"

Port = 9199
responseString = ""

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN, GPIO.OUT)
GPIO.output(PIN, GPIO.LOW)
if len(sys.argv) > 1:
  Port = int(sys.argv[1])
socket = socket(AF_INET, SOCK_DGRAM)
socket.bind(('',Port))

while 1:
  data, connection = socket.recvfrom(1024)
  if data[0] == LEDON:
    # enable LED
    print("LED on")
    GPIO.output(PIN, GPIO.HIGH)
    responseString = "LED enabled"
  elif data[0] == LEDOFF:
    # disable LED
    GPIO.output(PIN, GPIO.LOW)
    print("LED off")
    responseString = "LED disabled!"
  elif data[0] == LEDFLASH:
    # flash LED
    print("flash")
    responseString = "Flashing started!"
  else:
   responseString = data
  socket.sendto(responseString.encode("utf-8"), connection)
socket.close()
